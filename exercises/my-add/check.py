from gettext import gettext, textdomain
import subprocess
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
textdomain("hkis")


def check():
    output = subprocess.run(
        ["python3", "solution.py"],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
    )
    if output.stdout.strip() not in (
        "usage: python3 solution.py OP1 OP2",
        "usage: python3 ./solution.py OP1 OP2",
    ):
        message = gettext(
            "I expect you to print the usage line when no parameters are given."
        )

        if not output.stdout:
            checker.fail(message, gettext("(got nothing)"))
        checker.fail(
            message,
            gettext("Got:"),
            checker.code(output.stdout),
            gettext("Expected:"),
            checker.code("usage: python3 solution.py OP1 OP2"),
        )
    for i in range(1, 25, 7):
        for j in range(0, 25, 8):
            output = checker.run("solution.py", str(i), str(j))
            if str(i + j) not in output:
                checker.fail(
                    f"`./solution.py {i} {j}` should give `{i+j}`",
                    gettext("Got:"),
                    checker.code(output),
                )


if __name__ == "__main__":
    check()
