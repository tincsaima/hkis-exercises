Dans cet exercice d'introduction au tri en Python vous aurez deux
fonctions à implémenter.

Dans cet exercice nous représentons des étudiants par des paires
`(note, nom)`, donc des `tuple` de deux éléments.

Et nous représentons des classes d'étudiants par des listes de paires,
tel que :

```python
>>> students = [(85, "Susan Maddox"), (6, "Joshua Tran"), (37, "Jeanette Wafer")]
```


### Partie 1

Rédigez une fonction, nommée `sort_by_mark`, acceptant une liste
d'étudiants, et renvoyant une copie de cette liste triée par ordre
**décroissant** de **note**, exemple :

```python
>>> sort_by_mark(students)
[(85, "Susan Maddox"), (37, "Jeanette Wafer"), (6, "Joshua Tran")]
```

### Partie 2

Rédigez une fonction nommée `sort_by_name`, acceptant une liste d'étudints, et renvoyant une copie de cette liste triée par **nom**, exemple :

```python
>>> sort_by_name(students)
[(37, "Jeanette Wafer"), (6, "Joshua Tran"), (85, "Susan Maddox")]
```


# Conseils

Regardez le [Guide pour le tri](https://docs.python.org/fr/3/howto/sorting.html).
