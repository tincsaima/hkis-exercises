As an introduction to sorting lists in Python you'll have to implement
two functions.


In this exercise we represent students as a pair of `(mark,
full_name)`, so a tuple of two elements.

And in this exercises we represent students as lists of pairs, like:

```python
>>> students = [(85, "Susan Maddox"), (6, "Joshua Tran"), (37, "Jeanette Wafer")]
```

### Part 1

Write a function named `sort_by_mark` that take as argument a list of
students and returns a copy of it sorted by **mark** in **descending**
order. Such as:

```python
>>> sort_by_mark(students)
[(85, "Susan Maddox"), (37, "Jeanette Wafer"), (6, "Joshua Tran")]
```

### Part 2

Write a function named `sort_by_name` that take as argument a list of
 students and returns a copy of it sorted by **name** in **ascending**
 order, such as:

```python
>>> sort_by_name(students)
[(37, "Jeanette Wafer"), (6, "Joshua Tran"), (85, "Susan Maddox")]
```


# Advices

Take a look at the [Sorting howto](https://docs.python.org/3/howto/sorting.html).
