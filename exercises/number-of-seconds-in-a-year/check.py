import re
import sys
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    if not output:
        checker.fail(
            """Your code printed nothing, did you forgot to call the
[print](https://docs.python.org/3/library/functions.html#print) function?

As a reminder: to display for example the result of `60 × 60` the syntax is:

```python
print(60 * 60)
```
"""
        )
    numbers = re.findall(r"[0-9]+", output)
    if len(numbers) == 0 or len(numbers) > 1:
        checker.fail(
            "I expected a single number (the result).",
            "You printed:",
            checker.code(output),
        )
    number = int(numbers[0])
    if output.replace(" ", "") == "60*60*24*365":
        checker.fail(
            f"You're printing `{output}`, while I expect you to "
            "print the result of this.",
            "I bet you used quotes, which are used to buid strings, "
            "where you should have not, to let Python interpret the values:",
            "`print(2 * 2)` prints `4`.",
            '`print("2 * 2")` prints `2 * 2`.',
        )
    if number == 60 * 60 * 24 * 365:
        print(
            f"Looks good to me! {60 * 60 * 24 * 365} seconds per year, that's a lot!!"
        )
        sys.exit(0)
    if number == 60 * 60 * 24:
        checker.fail(
            f"You printed {60 * 60 * 24} (60 × 60 × 24), this is the number of "
            "seconds in a day. I need the number of seconds in a year of **365** days."
        )
    if number == 60 * 60 * 365:
        checker.fail(
            f"You printed {60 * 60 * 365} (60 × 60 × 365), this is the number of "
            "**hours** in a year. I need the number of **seconds** in a year."
        )
    message = ["This is not the correct answer. You printed:", checker.code(output)]
    if number > 60 * 60 * 24:
        days, rest = divmod(number, 60 * 60 * 24)
        if rest:
            message.append("It's roughly equivalent to {} days.".format(days))
        else:
            message.append(
                """It's the number of seconds in {} days
(I want the number of seconds in `365` days).""".format(
                    days
                )
            )
    checker.fail(*message)


if __name__ == "__main__":
    check()
