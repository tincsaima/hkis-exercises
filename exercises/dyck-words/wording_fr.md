Rédigez une fonction `is_a_dyck_word`, acceptant un seul argument
`word` (une chaîne):

```python
def is_a_dyck_word(word: str) -> bool:
    ...
```


# Qu’est-ce qu’un mot de Dyck?

=> [https://fr.wikipedia.org/wiki/Langage_de_Dyck](https://fr.wikipedia.org/wiki/Langage_de_Dyck)

Un mot de Dyck est un mot composé de deux symboles, typiquement `(` et
`)`. Un mot de Dyck doit être « correctement parenthésé ».

Quelques exemples :

```python
assert is_a_dyck_word("") is True
assert is_a_dyck_word("()") is True
assert is_a_dyck_word("(((())))") is True
assert is_a_dyck_word("()()()()") is True
assert is_a_dyck_word("()(())()") is True
assert is_a_dyck_word("(((") is False
assert is_a_dyck_word("((()") is False
assert is_a_dyck_word("()))") is False
assert is_a_dyck_word("()()()(") is False
```

Une chaîne contenant plus de deux symboles différents n’est pas valide, votre
fonction doit renvoyer `False` si le cas lui est présenté, par exemple :

```python
assert is_a_dyck_word("ABC") is False
```

Mais attention, votre implémentation doit accepter n’importe quelle
paire de symboles, donc :

```python
assert is_a_dyck_word("[]") is True
assert is_a_dyck_word("{}") is True
assert is_a_dyck_word("<>") is True
assert is_a_dyck_word("[[]]") is True
assert is_a_dyck_word("{{}}") is True
assert is_a_dyck_word("<<>>") is True
assert is_a_dyck_word("[][]") is True
assert is_a_dyck_word("{}{}") is True
assert is_a_dyck_word("<><>") is True
```

Vraiment n’importe quelle paire de symboles, tant que l’un joue le
rôle de « ouvrant » et le second le rôle de « fermant » :

```python
assert is_a_dyck_word("AB") is True
assert is_a_dyck_word("ABAB") is True
assert is_a_dyck_word("AABB") is True
assert is_a_dyck_word("AABBAB") is True
assert is_a_dyck_word("AAABBB") is True
assert is_a_dyck_word("ABABAB") is True
```

Mais les exemples suivants ne sont pas « correctement parenthésés » :

```python
assert is_a_dyck_word("AA") is False
assert is_a_dyck_word("BBBB") is False
assert is_a_dyck_word("AAAB") is False
assert is_a_dyck_word("AABAA") is False
assert is_a_dyck_word("ABBA") is False
assert is_a_dyck_word("ABBB") is False
```

Une dernière série juste pour bien insister sur le fait que tout
caractère peut jouer le rôle d'ouvrant et que tout caractère peut
jouer le rôle de fermant :

```python
assert is_a_dyck_word(",.") is True
assert is_a_dyck_word(",.,.") is True
assert is_a_dyck_word("..,,") is True
assert is_a_dyck_word("coco") is True
assert is_a_dyck_word("tutu") is True
assert is_a_dyck_word("papa") is True
assert is_a_dyck_word("titi") is True
assert is_a_dyck_word("toto") is True
```

Donc, oui, votre fonction devra déduire quel symbole « ouvre » et quel
symbole « ferme » dans le mot..

Astuce : Si vous vous sentez en difficulté, commencez par implémenter
une version simple n’acceptant que `(` et `)`, la moulinette de
correction vous dira si c’est juste.

Et, oui, deviner les symboles nous ammène à des situations étranges comme :

```python
assert is_a_dyck_word(")(") is True
```

Parce qu’ici le symbole « ouvrant » est `)` et le symbole « fermant »
est `(`. Étrange, mais valide.

Confidence : Je trouve ça réconfortant. D’abord j’étais déçu de
remarquer que lire "((()))" à l’envers donnait ")))(((". Mais avec
notre exercice, les deux sont des mots de Dyck.
