import os
from pathlib import Path

import django
import correction_helper as checker
from django.test import Client

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
Path("proj").mkdir()
Path("proj/__init__.py").touch()
Path("proj/urls.py").write_text(
    """
from django.urls import path
from solution import hello

urlpatterns = [
    path("", hello),
]
"""
)
Path("proj/settings.py").write_text(
    """
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = "xqn#6t6x*c_#x&&4=fht98=%d=1c40x2#o8gcq=@8smvs%6^+!"
DEBUG = True
ALLOWED_HOSTS = ["testserver"]
INSTALLED_APPS = []
MIDDLEWARE = []
ROOT_URLCONF = "proj.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = "/static/"
"""
)

os.environ["DJANGO_SETTINGS_MODULE"] = "proj.settings"
Path("proj/urls.py").write_text(
    """
from django.urls import path
from solution import hello

urlpatterns = [
    path('', hello),
]
"""
)
with checker.student_code():
    django.setup()
    client = Client()
    content = client.get("/").content.decode()
if content != "Hello Django":
    checker.fail("Expected `Hello Django`, got:", checker.code(content))
