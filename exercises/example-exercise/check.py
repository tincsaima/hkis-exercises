from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def main():
    with checker.student_code():
        from solution import blah
    for a_test in ["some", "tests"]:
        with checker.student_code():
            theirs = blah(a_test)
        if theirs is None:
            checker.fail("You are returning `None`, you should return a string.")
        if theirs != "blah":
            checker.fail(
                f"""Blah blah with blah `{a_test}` you failed,
expected `"blah"`, got: `{theirs!r}`"""
            )


if __name__ == "__main__":
    main()
