Rédigez une fonction, nommée `battery_charge`, représentant
graphiquement la charge d'une batterie.

Votre fonction ne doit rien renvoyer (donc renvoyer `None`, c'est ce
que Python renvoie par défaut lorsqu'une fonction n'a pas de
`return`.), elle doit uniquement afficher la barre, et la charge en
pourcent.

La fonction prend un `int` en paramètre, entre `0` et `100`.

La fonction affiche une barre, représentant le remplissage de la
batterie, sur une ligne, puis le pourcentage, sur une 2ème ligne.

La barre commence par `[`, se termine par `]`, et contient de 0 à 10
caraceres ❚ pour représenter visuellement le remplissage de la
batterie, le nombre de barres doit être
[arrondi](https://docs.python.org/fr/3/library/functions.html#round),
c'est à dire que pour 9% et 11% on va utiliser une seule barre.


## Exemples

```
>>> battery_charge(0)
[          ]
0%
>>> battery_charge(5)
[          ]
5%
>>> battery_charge(9)
[❚         ]
9%
>>> battery_charge(11)
[❚         ]
11%
>>> battery_charge(100)
[❚❚❚❚❚❚❚❚❚❚]
100%
```

Si le caractère `❚` vous pose problème (dans votre éditeur ou votre terminal) vous pouvez simplement utiliser un `|` à la place.


## Références

- La fonction native [round](https://docs.python.org/fr/3/library/functions.html#round).
