from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    try:
        output = int(output)
    except ValueError:
        if output == "":
            checker.fail(
                "I expected your code to `print` an integer, it printed nothing ☹"
            )
        else:
            checker.fail(
                "I expected your code to `print` an integer, it printed:",
                checker.code(output),
            )
    if output > 100000007:
        checker.fail("It's less, your code printed:", str(output))
    if output < 10000099:
        checker.fail("It's way more, like 10× more, your code printed:", str(output))
    if output < 100000007:
        checker.fail("It's more, your code printed:", str(output))
    their_code = Path("solution.py").read_text()
    if "100000007" in their_code and len(their_code) < 25:
        checker.fail("I see what you did here.")
    print(checker.congrats())


if __name__ == "__main__":
    check()
