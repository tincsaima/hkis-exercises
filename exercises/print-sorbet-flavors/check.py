import string
import sys
from collections import Counter
from itertools import combinations
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")

FLAVORS = [
    "Banana",
    "Chocolate",
    "Lemon",
    "Pistachio",
    "Raspberry",
    "Strawberry",
    "Vanilla",
]


def check():
    output = checker.run("solution.py")
    solution = Path("solution.py").read_text()
    if output == "":
        if "def " in solution:
            checker.fail("You printed nothing, maybe you forgot to call your function?")
        else:
            checker.fail(
                """You printed nothing, maybe forgot to call `print()`?

You're not in an interpreter, there's no implicit print here."""
            )
    old_expected = "\n".join(i + j for i, j in combinations(string.ascii_lowercase, 2))
    expected = "\n".join(f"{i}, {j}" for i, j in combinations(FLAVORS, 2))
    if output == expected or output == old_expected:
        sys.exit(0)  # Success!
    if "," not in output:
        checker.fail(
            """Please use comas between the flavors, got:""", checker.code(output)
        )
    if "," in output and ", " not in output:
        checker.fail(
            """Please add a space after the coma (between the flavors),
it's more readable. You printed:""",
            checker.code(output),
        )
    for i, line in enumerate(output.split("\n"), start=1):
        line = line.strip()
        if "," not in line:
            checker.fail(
                f"""On line {i} (`{line}`), there's no coma, it looks wrong.
Your full output:""",
                checker.code(output),
            )
        if ", " not in line:
            checker.fail(
                "You're missing a space after a coma in your output:",
                checker.code(output),
            )
        if line.count(",") > 1:
            checker.fail(
                f"Found more than one coma in `{line}`, full output is:",
                checker.code(output),
            )
        if "'" in line:
            checker.fail(
                f"""Found an quote (`'`) on line `{line}`,
I just want a coma and a space between flavors, no quotes. Full output is:""",
                checker.code(output),
            )
        if line.count(" ") > 1:
            checker.fail(
                f"Found more than one space in `{line}`, full output is:",
                checker.code(output),
            )
        left, right = line.split(", ")
        if left not in FLAVORS:
            checker.fail(
                f"""Beware, `{left}` is not a known flavor in this restaurant,
please stick to the given list, your full output is:""",
                checker.code(output),
            )
        if right not in FLAVORS:
            checker.fail(
                f"""Beware, `{right}` is not a known flavor in this restaurant,
please stick to the given list, your full output is:""",
                checker.code(output),
            )
        if left == right:
            checker.fail(
                f"On line {i} you're having: `{line}`.",
                "But I explicitly told to not list recipes with twice the same flavor.",
                "Your full output is:",
                checker.code(output),
            )

    their_pairs = set(frozenset(line.split(", ")) for line in output.split("\n"))
    my_pairs = set(frozenset(line.split(", ")) for line in expected.split("\n"))
    for left, right in my_pairs - their_pairs:
        checker.fail(
            f"""Why not proposing `{left}, {right}` (or `{right}, {left}`)?
Your full output is:""",
            checker.code(output),
        )
    for superfluous in their_pairs - my_pairs:
        checker.fail(
            f"Why proposing `{', '.join(superfluous)}`? You full output is:",
            checker.code(output),
        )
    common = Counter(
        [frozenset(line.split(", ")) for line in output.split("\n")]
    ).most_common()
    for (left, right), occurrence in common:
        if occurrence > 1:
            checker.fail(
                f"""You give the pair {left}, {right} twice
(once as `{left}, {right}` and once as `{right}, {left}`).

You should display this duo a single time (any version of it).

Your full output is:""",
                checker.code(output),
            )


if __name__ == "__main__":
    check()
