from pathlib import Path
from random import randint

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")

with checker.student_code():
    from solution import mul


def check():
    seed = randint(100, 100_000)
    for test, expected in (
        ((10, 10), 100),
        ((0, 1, 2, 3, 4, 5), 0),
        ((1, 1), 1),
        ((100, 100), 10_000),
        ((1, seed, 1), seed),
    ):
        with checker.student_code(
            print_hook=checker.print_to_admonition(
                f"When calling `mul({list(test)!r})` it printed:"
            )
        ):
            their = mul(list(test))
        if their != expected:
            checker.fail(
                f"Wrong answer for `mul({list(test)!r})`, expected your "
                f"function to `return {expected}`, but it returned `{their}`"
            )


if __name__ == "__main__":
    check()
