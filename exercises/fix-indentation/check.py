from gettext import gettext, textdomain
import re
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
textdomain("hkis")


EXPECTED = """Gonna knock three times:
*knock*
*knock*
*knock*
- Who's there?"""

CODE = """print("Gonna knock three times:")
for i in range(3):
print("*knock*")
print("- Who's there?")
"""


def check():
    solution = Path("solution.py").read_text(encoding="UTF-8")
    if not solution:
        checker.fail(
            gettext(
                "You emptied the code area, here's what there was, just copy paste it:"
            ),
            checker.code(
                CODE,
                "python",
            ),
        )
    output = checker.run("solution.py")
    output = re.sub(" *\n *", "\n", output)
    if output != EXPECTED:
        for line in EXPECTED.splitlines():
            if line not in output:
                trailer = f"Where, exactly, is the line `{line}`?"
                break
        else:
            trailer = ""
        checker.fail(
            "The code did not printed exactly what's the exercise is asking for, "
            "it printed:",
            checker.code(output),
            "While I expected:",
            checker.code(EXPECTED),
            trailer,
        )
    if solution.count("*knock*") > 1:
        checker.fail(
            "You modified the code, I see more than 1 `knock` in it now...",
            "You just need to fix indentation of the given code, not modify it.",
            "In case you want to copy-past it to start fresh, here it is:",
            checker.code(CODE, "python"),
        )
    print(checker.congrats())


if __name__ == "__main__":
    check()
