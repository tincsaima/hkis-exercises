from subprocess import run, PIPE
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.c")

gcc = run(
    ["gcc", "solution.c", "-o", "solution"], stderr=PIPE, stdout=PIPE, encoding="UTF-8"
)
if gcc.stderr:
    checker.fail("Failed compiling your code:", checker.code(gcc.stderr))

output = run(["./solution"], stdout=PIPE, stderr=PIPE, encoding="UTF-8")
if output.stdout == "Hello World\n":
    print(checker.congrats())
    exit(0)

if output.stdout == "Hello World":
    checker.fail("It would be better with a newline at the end, written as `\\n` in C.")

message = ["Naupe. Your code printed on `stdout`:", checker.code(output.stdout)]
if output.stderr:
    message.extend(["And on `stderr`:", checker.code(output.stderr)])
message.append("While I expected it to print: `Hello World\\n`")
checker.fail(*message)
